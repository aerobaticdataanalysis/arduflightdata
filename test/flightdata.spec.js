"use strict";
import chai from "chai";
import {
  Bin
} from 'ardubinreader';
import {RawData, Flight} from 'arduflightdata';
import dataForge from 'data-forge';
import fs from 'fs';
import { RADIANS } from "@thomasdavid/geometry-js";
const DataFrame = dataForge.DataFrame;

let expect = chai.expect
let should = chai.should()

//this will break in browser, to be expected at the moment
//TODO find a way to pass a buffer to the tests in the browser
const contents = fs.readFileSync('examples/00000060.BIN');
const rdat = RawData.fromBuffer(contents);
describe('test the RawData class', function () {
  
  it('should construct the class correctly', function () {
    expect(rdat._bin).to.be.instanceOf(Bin);
    console.log(rdat.field_lookup);
    expect(rdat.field_lookup['FMT'].name).to.be.equal('FMT');
  })

  it('should return a dataframe of a field', function () {
    const df = rdat.get_df('ATT');
    expect(df).to.be.instanceOf(DataFrame);
    console.log(df.first());
    expect(df.content.columnNames[0]).to.be.equal('TimeUS');
  })


  it('should get the first valid time', function(){
    expect(rdat.first_good_time).to.be.greaterThan(0);
  })

  it('should get the valid GPS data', function () {
    expect(rdat.gps.first().sats).to.be.greaterThan(5);
  })

  it('should have a non zero heading at the first data point', function() {
    expect(rdat.ekf.first().yaw / RADIANS).to.be.not.equal(0);
  }) 

  it('should give a home position at my flying field', function() {
    expect(rdat.home.pilot_gps.get_deg_lat()).to.be.closeTo(51.46,0.1);
    expect(rdat.home.pilot_gps.get_deg_long()).to.be.closeTo(-2.7914, 0.1);
  })

})

describe('test the Flight class', function () {

  it('should setup the class')

})