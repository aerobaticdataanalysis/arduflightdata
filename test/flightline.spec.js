"use strict";
import chai from "chai";
import {Box, FlightLine} from "arduflightdata";
import { GPSPosition } from "@thomasdavid/geometry-js";

let expect = chai.expect

describe('test the FlightLine Class', function () {
  
  it('should construct the class correctly', function () {
    //TODO break this test up a bit
    const fline = new FlightLine(
      new Box(new GPSPosition(51.46086477447531, -2.7917830489272517, 0, false), 0),
      new Box(new GPSPosition(51.45915340125264, -2.7909248780019067, 0, false), 145)
    )
    expect(fline.box_coord.origin.x).to.be.equal(fline.startup_box.pilot_gps.distance_to(fline.contest_box.pilot_gps).x);

  })

  it('should transform a point to the box coord')

  it('should transform an Euler rotation to the box coord')

})