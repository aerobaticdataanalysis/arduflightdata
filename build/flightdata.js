"use strict";

require("core-js/modules/es.array-buffer.slice");

require("core-js/modules/es.typed-array.uint8-array");

require("core-js/modules/es.typed-array.to-locale-string");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.FlightData = exports.RawData = void 0;

var _ardubinreader = require("ardubinreader");

var _dataForge = _interopRequireDefault(require("data-forge"));

var _geometryJs = require("@thomasdavid/geometry-js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const DataFrame = _dataForge.default.DataFrame;

function generate_field_lookup(fields) {
  const lookup = {};

  for (let i = 0; i < Object.keys(fields).length; i++) {
    lookup[fields[Object.keys(fields)[i]].name] = fields[Object.keys(fields)[i]];
  }

  return lookup;
}

class RawData {
  constructor(bin) {
    this._bin = bin;
    this._dfs = {};
    this.field_lookup = generate_field_lookup(this._bin._fields);
  }

  static fromBuffer(buf) {
    const _bin = new _ardubinreader.Bin(new Uint8Array(buf));

    _bin.parse_all();

    return new RawData(_bin);
  }

  get_df(name) {
    //lazy loading for the dataframes because there are a lot of useless ones, 
    //perhaps this is pointless as DataFrame is lazy loading itself.
    if (this._dfs[name] === undefined && !(this._bin._data[name] === undefined)) {
      const tempdf = new DataFrame(this._bin._data[name]);
      const new_names = {};

      for (let i = 0; i < this.field_lookup[name].labels.length; i++) {
        new_names[i] = this.field_lookup[name].labels[i];
      }

      this._dfs[name] = tempdf.renameSeries(new_names).setIndex('TimeUS');
    }

    return this._dfs[name];
  }

  get_dfs(names) {
    const dfs = [];

    for (let i = 0; i < names.length; i++) {
      dfs.push(this.get_df(names[i]));
    }

    return dfs;
  }

  join_dfs(names) {
    const dfs = this.get_dfs(names);
    return DataFrame.merge(dfs);
  }

  get_parms() {
    return this._bin.get_parms();
  }

  get_GPS() {
    //select only data with a 3D fix
    return this.get_df('GPS').where(row => row.Status === 3).select(row => {
      return {
        lat: _geometryJs.RADIANS * row.Lat,
        lng: _geometryJs.RADIANS * row.Lng,
        alt: row.Alt,
        sats: row.NSats
      };
    });
  }

}

exports.RawData = RawData;

class FlightData {
  constructor(buf) {
    console.log('reading log');
    this.rawdata = RawData.fromBuffer(buf);
    console.log('getting gps data');
    this.GPS = this.rawdata.get_GPS();
    console.log('getting gps home');
    const gpshome = this.GPS.first();
    this.GPSHome = new _geometryJs.GPSPosition(gpshome.lat, gpshome.lng, gpshome.alt);
  }

}

exports.FlightData = FlightData;