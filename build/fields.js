"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.fields = fields;

// This is WIP thoughts about how to manage units etc. 
class Col {
  constructor(field, name, unit) {
    this.field = field;
    this.name = name;
    this.unit = unit;
  }

}

const ekfsource = {
  1: 'EKF',
  2: 'XKF',
  3: 'NKF'
};

function fields(ekfv) {
  //ekfv=parms['AHRS_EKF_TYPE']
  const ekf1 = ekfsource[ekfv] + '1';
  const ekf2 = ekfsource[ekfv] + '2';
  return {
    'N': Col(ekf1, 'PN', 'm'),
    'E': Col(ekf1, 'PE', 'm'),
    'D': Col(ekf1, 'PD', 'm'),
    'VN': Col(ekf1, 'VN', 'm/s'),
    'VE': Col(ekf1, 'VE', 'm/s'),
    'VD': Col(ekf1, 'VD', 'm/s'),
    'r': Col(ekf1, 'Roll', 'deg'),
    'p': Col(ekf1, 'Pitch', 'deg'),
    'yw': Col(ekf1, 'Yaw', 'deg'),
    'wN': Col(ekf2, 'VWN', 'm/s'),
    'wE': Col(ekf2, 'VWN', 'm/s')
  };
}