"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "FlightData", {
  enumerable: true,
  get: function get() {
    return _flightdata.FlightData;
  }
});
Object.defineProperty(exports, "RawData", {
  enumerable: true,
  get: function get() {
    return _flightdata.RawData;
  }
});

var _flightdata = require("./flightdata.js");