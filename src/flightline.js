
import {Coord, GPSPosition, Point, RADIANS} from '@thomasdavid/geometry-js';

function boxpoints(length) {
  return [
    new Point(0, 0, 0),
    new Point(length, 0, 0),
    new Point(length, length * Math.tan(RADIANS * 60), 0),
    new Point(0, 0, 0),
    new Point(length, 0, 0),
    new Point(length, - length * Math.tan(RADIANS * 60), 0),
    new Point(0, 0, 0)
  ]
}

export class Box {
  constructor(pilot_gps, pilot_heading) {
    this.pilot_gps = pilot_gps;
    this.pilot_heading = pilot_heading;
  }
}

export class FlightLine {
  constructor(startup_box, contest_box) {
    this.startup_box = startup_box;
    this.contest_box = contest_box; 
    this.base_coord = Coord.from_nothing();
    this._box_coord = undefined;
  }

  get box_coord() {    
    if (this._box_coord===undefined) {
      this._box_coord = Coord.from_nothing().translate(
        this.startup_box.pilot_gps.distance_to(new GPSPosition(this.contest_box.pilot_gps.lat, this.contest_box.pilot_gps.long, this.startup_box.pilot_gps.alt))
        ).euler_rotate(
          new Point(0, 0, this.contest_box.pilot_heading)
          );
    }
    return this._box_coord;    
  }

  get geojson() {
      const worldpoints = boxpoints(150).map(this.point_to_box.bind(this));
      const worldgpspositions = worldpoints.map(this.startup_box.pilot_gps.move);
      return worldgpspositions.map(gpspos => [gpspos.get_deg_long(), gpspos.get_deg_lat()]);
  }


  point_to_box(point){
    return point.rotate(this.box_coord.rotation_matrix()).translate(this.box_coord.origin);
  }
  
  euler_to_box(euler){
    return new Point(euler.x, euler.y, euler.z + this.contest_box.pilot_heading);
  }

}