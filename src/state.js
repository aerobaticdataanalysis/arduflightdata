import {Point, Coord, GPSPosition} from '@thomasdavid/geometry-js';


export class State {
  constructor(props) {
    this.position = new Point(props.x, props.y, props.z);
    this.euler = new Point(props.roll, props.pitch, props.yaw);
    this.gps = new GPSPosition(props.lat, props.lng, props.alt);
    this.wind = new Point(props.xwind, props.ywind, 0); 
  }

  static from_ekf(flightline, ekf){
    const posin = new Point(ekf.x, ekf.y, ekf.z);
    const windin = new Point(ekf.xwind, ekf.ywind, 0);

    const pos = flightline.point_to_box(posin);
    const eul = flightline.euler_to_box(new Point(ekf.roll, ekf.pitch, ekf.yaw));
    const gps = flightline.startup_box.pilot_gps.move(posin);
    const wind = flightline.point_to_box(windin);
  
    return new State({
      x: pos.x, y: pos.y, z: pos.z,
      roll: eul.x, pitch: eul.y, yaw: eul.z,
      lat: gps.lat, lng: gps.long, alt: gps.alt,
      xwind: wind.x, ywind: wind.y, zwind: wind.z
    });
  }

  to_dict() {
    return {
      x: this.position.x, y: this.position.y, z: this.position.z,
      roll: this.euler.x, pitch: this.euler.y, yaw: this.euler.z,
      lat: this.gps.lat, lng: this.gps.long, alt: this.gps.alt,
      xwind: this.wind.x, ywind: this.wind.y, zwind: this.wind.z
    };
  }

  get coord() {
    if (this._coord === undefined) {
      this._coord = Coord.from_nothing().euler_rotate(this.euler);
    }
    return this._coord;
  }

  get heading() {
    if (this._heading === undefined) {

      this._heading = Math.atan(this.coord.x_axis.y/ this.coord.x_axis.x);
    }
    return this._heading;
  }

}