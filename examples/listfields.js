import {
  FlightData, RawData
} from 'arduflightdata';
import fs from 'fs';

const buf = fs.readFileSync('./examples/00000060.BIN')

const fdat = RawData.fromBuffer(buf);

for (const [key, value] of Object.entries(fdat.field_lookup)) {
  console.log(`${key}, labels, ${value.labels}`);
  console.log(`${key}, mults, ${value.mults}`);
  console.log(`${key}, units, ${value.units}`);
}

//for (let i=0; i<fdat.)
//console.log()
